# Mean, Median, and Mode

## Problem

In this challenge, we practice calculating the mean, median, and mode.

Given an array, `X`, of `N` integers, calculate and print the respective mean, median, and mode on separate lines. If your array contains more than one modal value, choose the numerically smallest one.

### Input Format

The first line contains an integer, `N`, denoting the number of elements in the array.
The second line contains `N` space-separated integers describing the array's elements.

### Constraints

- 10 <= N <= 2500
- 0 <= X[i] <= 10^5
- The username consists only of uppercase and lowercase letters.

### Output Format

Print 3 lines of output in the following order:

1. Print the mean on a new line
2. Print the median on a new line
3. Print the mode on a new line; if more than one such value exists, print the numerically smallest one.

#### Sample Input

```s
10
64630 11735 14216 99233 14470 4978 73429 38120 51135 67060
```

#### Sample Output

```s
43900.6
44627.5
4978
```

#### Explanation

We sum all `N` elements in the array, divide the sum by `N`, and print our result on a new line.

To calculate the median, we need the elements of the array to be sorted in either non-increasing or non-decreasing order. We then average the two middle elements, and print our result on a new line.

Every number occurs once, making 1 the maximum number of occurrences for any number in `X` because we have multiple values to choose from, we want to select the smallest one, 4978, and print it on a new line.

## Source

[HackerRank - Mean, Median, and Mode](https://www.hackerrank.com/challenges/s10-basic-statistics/problem)