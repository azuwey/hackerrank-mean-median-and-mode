/**
 * Author: Zarandi David (Azuwey)
 * Date: 02/26/2019
 * Source: HackerRank - https://www.hackerrank.com/challenges/s10-basic-statistics/problem
 **/
#include <algorithm>
#include <cmath>
#include <cstdio>
#include <iostream>
#include <map>
using namespace std;

int main() {
  int n;
  cin >> n;

  // Creating a dictionary from the input
  map<int, int> arr;
  for (int i = 0; i < n; ++i) {
    int inp;
    cin >> inp;
    if (arr.find(inp) != arr.end()) {
      ++arr[inp];
    } else {
      arr[inp] = 1;
    }
  }

  // Pre setup variables for the calculations
  double mean, median = 0;
  pair<int, int> mode = {-1, -1};
  bool double_check = n % 2 == 0;
  int i = n / 2;

  for_each(arr.begin(), arr.end(), [&](pair<int, int> p) {
    // Mode selection
    if (p.second > mode.second) {
      mode.first = p.first;
      mode.second = p.second;
    }

    // Mean calculation
    mean += (p.second * p.first);

    // Median calculation
    if (double_check && i <= 1 && i >= 0 && p.second == 1) {
      median += p.first;
    } else if (double_check && i <= 1 && i >= 0 && p.second > 1) {
      median = 2 * mode.first;
    } else if (!double_check && i == 1) {
      median = p.first;
    }
    i -= p.second;
  });

  // Check n is even or odd
  (double_check) && (median /= 2);

  cout << (mean / n) << endl << median << endl << mode.first << endl;
  return 0;
}
